import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { add, edit, selectTodos, activateEdit, activateEditWithValue, updateInputValue, clearState, removeTodo, markAsComplete } from './todoSlice'
import { useForm } from "react-hook-form";

export const Todos = () => {
  const { todoList, isNew, isInput, inputValue, isEdit, todoId } = useSelector(selectTodos);
  const [name, setName] = useState('');
  const [all, setAll] = useState(true); // flag to list all the todos
  const [isCompleted, setIsCompleted] = useState(false); // flag to list completed todos
  const [inProcess, setInProcess] = useState(false); // flag to list todos which are not completed
  const dispatch = useDispatch();
  const { register, handleSubmit, formState: { errors } } = useForm();

  useEffect(() => {
    clearState()
  })

  const onSubmit = (data) => {
      if(Object.keys(data)[0] === 'newTodo') {
        dispatch(add({name: data.newTodo}))
      }

      if(Object.keys(data)[0] === 'editTodo') {
        dispatch(edit({name: data.editTodo, id: todoId}))
      }

  }

    return (
        <div>
            <div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    {isNew ? // if isNew flag is enable input field is disable and New button is enable
                        <button
                        onClick={() => { dispatch(activateEdit()) }}
                        >
                            New
                        </button>
                        : null    
                    }
                    {isInput ? // this will enable input field and disable New button
                        <div>
                            {isEdit ? // if this flag is enable the user is editing the existing todo
                                <div>
                                    <div>
                                    <input
                                    type="text"
                                    value={inputValue}
                                    {...register('editTodo', { pattern: /^[A-Za-z@.]+$/ })}
                                    /> 
                                    <input type="submit" />
                                    </div>
                                </div>
                                :
                                null
                            }
                            {!isEdit ? // if this flag is NOT enable the user is creating new todo
                                <div>
                                    <input
                                    type="text"
                                    {...register('newTodo', { pattern: /^[A-Za-z@.]+$/ })}
                                    />
                                    <input type="submit" />
                                </div>
                                : null
                            }
                            
                        </div> : null
                    }
                    <p>{errors.editTodo && <span> Input can not contain numbers </span>}</p>
                    <p>{errors.newTodo && <span> Input can not contain numbers </span>}</p>
                </form>
            </div>
            <div>
                <div>
                    <button onClick={() => {setAll(true); setIsCompleted(false); setInProcess(false) }}>All</button>
                    <button onClick={() => {setAll(false); setIsCompleted(true); setInProcess(false) }}>Completed</button>
                    <button onClick={() => {setAll(false); setIsCompleted(false); setInProcess(true) }}>In Process</button>
                </div>
                {all ? <div> {todoList.map((todo) => ( //display all the todos
                    <div>
                        {todo.name}
                        {console.log(isNew)}
                        {!todo.isCompleted ? 
                            <button onClick={() => { setName(todo.name);  dispatch(activateEditWithValue({ name: todo.name, id: todo.id }))}}>
                                Edit
                            </button> : null }
                        {!todo.isCompleted ? 
                            <button onClick={() => dispatch(markAsComplete(todo.id))}>
                                Complete
                            </button> : null
                        }
                        <button onClick={() => dispatch(removeTodo(todo.id))}>
                            Delete
                        </button>
                    </div>
                    
                ))} </div> : null }
                {isCompleted ? <div> {todoList.map((todo) => ( // display only completed todos
                    <div>
                        {todo.isCompleted ? 
                            <div>
                                {todo.name}
                                {console.log(isNew)}
                                {/* <button onClick={() => { setName(todo.name);  dispatch(activateEditWithValue({ name: todo.name, id: todo.id }))}}>
                                    Edit
                                </button> */}
                                <button onClick={() => dispatch(removeTodo(todo.id))}>
                                    Delete
                                </button>
                            </div>
                        : null}
                    </div>
                    
                ))} </div> : null }
                {inProcess ? <div> {todoList.map((todo) => ( // display todos which are not completed
                    <div>
                        {!todo.isCompleted ? 
                            <div>
                                {todo.name}
                                {console.log(isNew)}
                                <button onClick={() => { setName(todo.name);  dispatch(activateEditWithValue({ name: todo.name, id: todo.id }))}}>
                                    Edit
                                </button>
                                <button onClick={() => dispatch(markAsComplete(todo.id))}>
                                    Complete
                                </button>
                                <button onClick={() => dispatch(removeTodo(todo.id))}>
                                    Delete
                                </button>
                            </div>
                        : null}
                    </div>
                    
                ))} </div> : null }
                
            </div>
        </div>
    )
}
